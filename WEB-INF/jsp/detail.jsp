<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="sample.*" %>
<jsp:useBean id="restaurantInformation" class="sample.RestaurantInformation" scope="session" />

<!DOCTYPE html charset="UTF-8">

<html>

<head>
<title>test</title>
</head>

<body>
<%
    List<Restaurant> restaurants = restaurantInformation.getRestaurants();
    int index = Integer.parseInt(request.getParameter("index"));

    Restaurant restaurant = restaurants.get(index);
    out.println("<p>" + restaurant.getName() + "</p>");
    out.println("<p><img src=" + restaurant.getShopImage2() + "></p>");
    out.println("<p>" + restaurant.getAddress() + "</p>");
    out.println("<p>Tel : " + restaurant.getTel() + "</p>");
    out.println("<p>営業時間<br>" + restaurant.getOpentime() + "</p>");
%>

<p><a href="/toy-app">top</a><br></p>

</body>
</html>
