<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="sample.*" %>
<jsp:useBean id="restaurantInformation" class="sample.RestaurantInformation" scope="session" />

<!DOCTYPE html charset="UTF-8">

<html>

<head>
<title>test</title>
</head>

<body>
<%
    List<Restaurant> restaurants = restaurantInformation.getRestaurants();
    int hitCount = restaurantInformation.getHitCount();
    int hitPerPage = restaurantInformation.getHitPerPage();
    int pageOffset = restaurantInformation.getPageOffset();
    int hitPageCount = ((hitCount - 1) / hitPerPage) + 1;
%>

<%
    int from = (pageOffset - 1) * hitPerPage + 1;
    int to = Math.min(pageOffset * hitPerPage, hitCount);
    out.print(hitCount + "件中  " + from + " - " + to + "件目<br>");
    for (int i = 0; i < hitPerPage; i++) {
        out.println("<form name=detail" + i + " method=post action=detail>");
        out.println("<input type=hidden name=detail value=true>");
        out.println("<input type=hidden name=index value=" + i + ">");
        out.println("</form>");
    }
    for (int i = 0; i < to - from + 1; i++) {
        Restaurant restaurant = restaurants.get(i);
        out.println("<p>");
        out.print("<a href=javascript:document.detail" + i + ".submit()>");
        out.print(restaurant.getName());
        out.print("</a>");
        out.println("<br>");
        out.print("アクセス：");
        out.print(restaurant.getLine());
        out.print(" ");
        out.print(restaurant.getStation());
        out.print(" ");
        out.print(restaurant.getStationExit());
        out.print(" ");
        out.print(restaurant.getWalk() + "分");
        out.println("<br>");
        out.println("<img src=" + restaurant.getShopImage1() + ">");
        out.println("</p>");
    }
%>

<%
    int pageLinkCount = 9;
    int minPage;
    int maxPage;
    if (hitPageCount <= pageLinkCount) {
        minPage = 1;
        maxPage = hitPageCount;
    } else {
        minPage = pageOffset - (pageLinkCount / 2);
        maxPage = pageOffset + ((pageLinkCount - 1) / 2);
    }
    if (minPage < 1) {
        int diff = 1 - minPage;
        minPage += diff;
        maxPage += diff;
    }
    if (maxPage > hitPageCount) {
        int diff = maxPage - hitPageCount;
        minPage -= diff;
        maxPage -= diff;
    }
    if (minPage < 1) {}
    for (int i = minPage; i <= maxPage; i++) {
        out.println("<form name=page" + i + " method=post action=search>");
        out.println("<input type=hidden name=paging value=true>");
        out.println("<input type=hidden name=page value=" + i + ">");
        out.println("</form>");
    }
    for (int i = minPage; i <= maxPage; i++) {
        if (i > minPage) {
            out.print(" ");
        }
        if (i != pageOffset) {
            out.print("<a href=javascript:document.page" + i + ".submit()>" + i);
            out.print("</a>");
        } else {
            out.print(i);
        }
    }
%>

<form name=previous method=post action=search>
<input type=hidden name=paging value=true>
<input type=hidden name=page value=<%= pageOffset - 1 %>>
</form>

<form name=next method=post action=search>
<input type=hidden name=paging value=true>
<input type=hidden name=page value=<%= pageOffset + 1 %>>
</form>

<p>
<%
    if (pageOffset > 1) {
        out.println("<a href=javascript:document.previous.submit()>前へ</a>");
    } else {
        out.println("前へ");
    }
    out.print("    ");
    if (pageOffset < (((hitCount - 1) / hitPerPage) + 1)) {
        out.println("<a href=javascript:document.next.submit()>次へ</a>");
    } else {
        out.println("次へ");
    }
%>
</p>

<p><a href="/toy-app">top</a><br></p>

</body>
</html>
