package sample;
import com.fasterxml.jackson.databind.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.net.*;

public class SampleServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // forward to the detail page when requested
        if (request.getParameter("detail") != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/detail.jsp");
            dispatcher.forward(request, response);
            return;
        }
        // set base uri
        if (request.getParameter("search") != null) {
            setBaseURI(request);
        }
        // set restaurant information
        if (!setRestaurantInformation(request)) {
            // if failed, forward to the error page
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
            dispatcher.forward(request, response);
            return;
        }
        // forward to the result page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/result.jsp");
        dispatcher.forward(request, response);
    }

    private static void setBaseURI(HttpServletRequest request) {
        // アクセスキー
        String acckey = request.getParameter("keyid");
        // 緯度
        // String lat = "35.670082";
        String lat = request.getParameter("latitude");
        // 経度
        // String lon = "139.763267";
        String lon = request.getParameter("longitude");
        // 範囲
        String range = request.getParameter("range");
        // 返却形式
        String format = "json";
        // エンドポイント
        String gnaviRestUri = "https://api.gnavi.co.jp/RestSearchAPI/20150630/";
        String prmFormat = "?format=" + format;
        String prmKeyid = "&keyid=" + acckey;
        String prmLat = "&latitude=" + lat;
        String prmLon = "&longitude=" + lon;
        String prmRange = "&range=" + range;
 
        // URI組み立て
        StringBuffer uri = new StringBuffer();
        uri.append(gnaviRestUri);
        uri.append(prmFormat);
        uri.append(prmKeyid);
        uri.append(prmLat);
        uri.append(prmLon);
        uri.append(prmRange);

        // save base uri
        HttpSession session = request.getSession();
        session.setAttribute("base", uri.toString());
    }

    // return true when success
    private static boolean setRestaurantInformation(HttpServletRequest request) throws IOException {
        // start or get session
        HttpSession session = request.getSession();

        // base uri
        StringBuffer uri = new StringBuffer();
        uri.append((String)session.getAttribute("base"));

        // offset_page
        if (request.getParameter("paging") != null) {
            String offsetPage = request.getParameter("page");
            uri.append("&offset_page=" + offsetPage);
        }

        // get information with json format
        URL restSearch = new URL(uri.toString());
        HttpURLConnection http = (HttpURLConnection)restSearch.openConnection();
        http.setRequestMethod("GET");
        http.connect();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nodeList = mapper.readTree(http.getInputStream());

        // save restaurant information
        if (checkRestaurantInformation(nodeList)) {
            RestaurantInformation restaurantInformation = new RestaurantInformation(nodeList);
            session.setAttribute("restaurantInformation", restaurantInformation);
            return true;
        }

        // failed to get information
        return false;
    }

    // check the received information
    private static boolean checkRestaurantInformation(JsonNode nodeList) {
        if (nodeList == null) {
            return false;
        }
        if (nodeList.path("total_hit_count").asText().equals("")) {
            return false;
        }
        return true;
    }
}
