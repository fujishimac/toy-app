package sample;
import com.fasterxml.jackson.databind.*;
import java.util.*;

public class RestaurantInformation {

    private int hitCount;
    private int hitPerPage;
    private int pageOffset;
    private List<Restaurant> restaurants;

    public RestaurantInformation() {}

    public RestaurantInformation(JsonNode nodeList) {
        // set hitCount
        this.hitCount = nodeList.path("total_hit_count").asInt();

        // set hitPerPage
        this.hitPerPage = nodeList.path("hit_per_page").asInt();

        // set pageOffset
        this.pageOffset = nodeList.path("page_offset").asInt();

        // set restaurants
        this.restaurants = new ArrayList<Restaurant>();
        List<Restaurant> restaurants = this.restaurants;
        Iterator<JsonNode> it = nodeList.path("rest").iterator();
        while (it.hasNext()) {
            Restaurant restaurant = new Restaurant(it.next());
            restaurants.add(restaurant);
        }
    }

    public List<Restaurant> getRestaurants() {
        return this.restaurants;
    }

    public int getHitCount() {
        return this.hitCount;
    }

    public int getHitPerPage() {
        return this.hitPerPage;
    }

    public int getPageOffset() {
        return this.pageOffset;
    }
}
