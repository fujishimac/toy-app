package sample;
import com.fasterxml.jackson.databind.*;

public class Restaurant {

    private String id;
    private String name;
    private String shopImage1;
    private String shopImage2;
    private String address;
    private String tel;
    private String opentime;
    private String line;
    private String station;
    private String stationExit;
    private String walk;

    public Restaurant(JsonNode nodeList) {
        this.id = nodeList.path("id").asText();
        this.name = nodeList.path("name").asText();
        JsonNode imageURL = nodeList.path("image_url");
        this.shopImage1 = imageURL.path("shop_image1").asText();
        this.shopImage2 = imageURL.path("shop_image2").asText();
        this.address = nodeList.path("address").asText();
        this.tel = nodeList.path("tel").asText();
        this.opentime = nodeList.path("opentime").asText();
        JsonNode access = nodeList.path("access");
        this.line = access.path("line").asText();
        this.station = access.path("station").asText();
        this.stationExit = access.path("station_exit").asText();
        this.walk = access.path("walk").asText();
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getShopImage1() {
        return this.shopImage1;
    }

    public String getShopImage2() {
        return this.shopImage2;
    }

    public String getAddress() {
        return this.address;
    }

    public String getTel() {
        return this.tel;
    }

    public String getOpentime() {
        return this.opentime;
    }

    public String getLine() {
        return this.line;
    }

    public String getStation() {
        return this.station;
    }

    public String getStationExit() {
        return this.stationExit;
    }

    public String getWalk() {
        return this.walk;
    }
}
